# INF-5101C - 3D Image Synthesis
## Content

This school project asks us to implement the following techniques in C#:

* Basic Phong illumination model
* Textures
* Bump maps
* 3D Spheres and Rectangles
* Zbuffer
* Raycasting algorithm for light and shadows
* Basic VPLs

## Building
Requires mono for linux to be installed.

Type the following in your Linux terminal
```
make && ./build/3D_synthesis_project
```

## Author
Bertrand Le Mée