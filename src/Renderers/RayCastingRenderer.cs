using System.Collections.Generic;
using System.Linq;

namespace ImageSynthesis
{
	class RayCastingRenderer
		: Renderer
	{
		public V3 ViewPoint {get; set;}
		public List<GeometricObject> ObjectList {get; set;}
		public PhongIlluminationModel Phong {get; set;}

		public RayCastingRenderer(PhongIlluminationModel phong, V3 viewPoint)
		{
			ObjectList = new List<GeometricObject>();
			Phong = phong;
			ViewPoint = viewPoint;
		}

		public void Render(List<GeometricObject> objectList, PhongIlluminationModel phong)
		{
			ObjectList = objectList;

			for(int x = 0; x < ScreenBitmap.Width; x++){
				for(int y = 0; y < ScreenBitmap.Height; y++){
					V3 origin = new V3(x, 0, y);
					V3 viewVector = origin - ViewPoint;
					Beam beam = new Beam(ViewPoint, viewVector);
					Color pixColor = RayCast(beam);

					ScreenBitmap.DrawPixel(
							x
							, ScreenBitmap.Height - y
							, pixColor
							);
				}
			}
		}

		private Color RayCast(Beam beam)
		{
			float intersectingDistance = float.MaxValue;
			GeometricObject obj = beam.FirstIntersectedObject(ObjectList, out intersectingDistance);

			if(obj != null)
			{
				V3 intersectingPoint = beam.Origin
					+ (beam.Direction * intersectingDistance);
				return PointColor(obj, intersectingPoint);
			}

			return new Color(0, 0, 0);
		}

		protected Color PointColor(GeometricObject obj, V3 intersectingPoint)
		{
			V2 intersectingUVCoord = obj.UVCoordinatesFromPoint(intersectingPoint);

			return Phong.LightColor(ObjectList, obj, intersectingPoint, intersectingUVCoord)
				* obj.PointColor(intersectingUVCoord);
		}
	}
}
