using System.Collections.Generic;

namespace ImageSynthesis
{
	interface Renderer
	{
		void Render(List<GeometricObject> objectList, PhongIlluminationModel phong);
	}
}
