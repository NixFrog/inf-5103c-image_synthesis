using System.Collections.Generic;

namespace ImageSynthesis
{
	class ZBufferRenderer
		: Renderer
	{
		private float[,] DepthBuffer;
		private const float UV_STEP = 0.001f;

		public ZBufferRenderer()
		{
			DepthBuffer = new float[ScreenBitmap.Width, ScreenBitmap.Height];
			ClearDepthBuffer();
		}

		public void ClearDepthBuffer()
		{
			for(int i = 0; i < DepthBuffer.GetLength(0); i++){
				for(int j = 0; j < DepthBuffer.GetLength(1); j++){
					DepthBuffer[i, j] = float.PositiveInfinity;
				}
			}
		}

		public void Render(List<GeometricObject> objectList, PhongIlluminationModel phong)
		{
			foreach(var obj in objectList){
				for(float u = 0.0f; u < 1.0f; u += UV_STEP){
					for(float v = 0.0f; v < 1.0f; v += UV_STEP){
						V2 objUV = new V2(u, v);
						V3 objPoint = obj.GetPointFromUV(objUV);

						int xPixel = (int)objPoint.x;
						int yPixel = (int)(ScreenBitmap.Height - objPoint.z);

						bool pointIsVisible = PointIsVisible(xPixel, yPixel, objPoint.y);

						if(pointIsVisible){
							ColorPixelFromObject(xPixel, yPixel, obj, objPoint, objUV, phong);
						}
					}
				}
			}
		}

		public bool PointIsVisible(int xPixel, int yPixel, float depth)
		{
			if(xPixel >= 0 && xPixel < ScreenBitmap.Width
				&& yPixel >= 0 && yPixel < ScreenBitmap.Height
				&& depth < DepthBuffer[xPixel, yPixel]
			){
				return true;
			}
			return false;
		}

		private void ColorPixelFromObject(int xPixel, int yPixel, GeometricObject obj, V3 objPoint, V2 uvCoords, PhongIlluminationModel phong)
		{
			DepthBuffer[xPixel, yPixel] = objPoint.y;
			var pointColor = obj.PointColor(uvCoords);
			pointColor *= phong.LightColor(
					new System.Collections.Generic.List<GeometricObject>()
					, obj, objPoint, uvCoords
					);
			ScreenBitmap.DrawPixel(xPixel, yPixel, pointColor);
		}
	}
}
