using System.Collections.Generic;
using System.Linq;

namespace ImageSynthesis
{
	class VPLRenderer
		: RayCastingRenderer
		, Renderer
	{
		public VPLRenderer(PhongIlluminationModel phong, V3 viewPoint)
			: base(phong, viewPoint)
		{}

		public new void Render(List<GeometricObject> objectList, PhongIlluminationModel phong)
		{
			ObjectList = objectList;
			var pointLightList = GeneratePointLights(phong.LightSources);

			Phong.LightSources.AddRange(pointLightList.ConvertAll(x => (LightSource)x));
			base.Render(objectList, Phong);
		}

		public List<PointLight> GeneratePointLights(List<LightSource> lightSources)
		{
			var pointLightList = new List<PointLight>();

			foreach(var light in lightSources.OfType<PointLight>()){
				var beamList = new List<Beam>();
				var pointList = SphereMaths.GenerateSphericPoints(light.Position, 500);

				foreach(var point in pointList){
					var direction = light.Position - point;
					var beam = new Beam(light.Position, direction);
					beamList.Add(beam);
				}

				foreach(var beam in beamList){
					var pointLight = GenerateSingleLight(beam);
					if(pointLight != null){
						pointLightList.Add(pointLight);
					}
				}
			}

			return pointLightList;
		}

		public PointLight GenerateSingleLight(Beam beam)
		{
			float intersectingDistance = float.MaxValue;
			PointLight pointLight = null;

				var direction = beam.Direction;
			var obj = beam.FirstIntersectedObject(
					ObjectList
					, out intersectingDistance
					);

			if(obj != null){
				V3 intersectingPoint = beam.Origin
					+ (beam.Direction * intersectingDistance)
					;
				float factor = 1f/500f;
				ColorFactor cf = new ColorFactor(factor, factor, factor);
				Color lightColor = PointColor(obj, intersectingPoint)*cf;
				//System.Console.WriteLine(direction.x + " "+ direction.y +" "+ direction.z);
				if(obj is Rectangle){
					return null;
					//lightColor = new Color(0,0,1);
				}
				if(obj is Sphere){
					//return null;
					System.Console.WriteLine(intersectingDistance.ToString());
					lightColor=new Color(1,0,0);
				}
				else{
					lightColor=new Color(0,1,0);
				}
				pointLight = new PointLight(lightColor, intersectingPoint, obj);
			}

			return pointLight;
		}
	}
}
