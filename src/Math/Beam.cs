using System.Linq;
using System.Collections.Generic;

namespace ImageSynthesis
{
	class Beam
	{
		public V3 Origin {get; set;}
		public V3 Direction {get; set;}
		public GeometricObject OriginObject {get; set;}

		public Beam(V3 origin, V3 direction, GeometricObject originObject = null)
		{
			Origin = origin;
			Direction = direction;
			OriginObject = originObject;
		}

		public GeometricObject FirstIntersectedObject(
				List<GeometricObject> objectList
				, out float intersectingDistance
				)
		{
			GeometricObject firstIntersectedObject = null;
			intersectingDistance = float.MaxValue;

			foreach(var obj in objectList.Where(
						(item, index) => item != OriginObject)){
				float distance = float.MaxValue;
				Direction.Normalize();
				bool isIntersected = obj.IsIntersectingRay(this, out distance);

				if(isIntersected && distance < intersectingDistance)
				{
					firstIntersectedObject = obj;
					intersectingDistance = distance;
				}
			}
			return firstIntersectedObject;
		}
	}
}
