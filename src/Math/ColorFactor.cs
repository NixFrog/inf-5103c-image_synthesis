namespace ImageSynthesis
{
	class ColorFactor
	{
		public float R {get; set;}
		public float G {get; set;}
		public float B {get; set;}

		public ColorFactor(float r, float g, float b)
		{
			R = r;
			G = g;
			B = b;
		}

		public static Color operator *(Color c, ColorFactor cf)
		{
			return new Color(
					c.R * cf.R
					, c.G * cf.G
					, c.B * cf.B
					);
		}

		public static Color operator *(ColorFactor cf, Color c)
		{
			return new Color(
					c.R * cf.R
					, c.G * cf.G
					, c.B * cf.B
					);
		}

		public static ColorFactor operator +(ColorFactor a, ColorFactor b)
		{
			return new ColorFactor(a.R + b.R, a.G + b.G, a.B + b.B);
		}

		public static ColorFactor operator *(ColorFactor cf, float f)
		{
			return new ColorFactor(cf.R * f, cf.G * f, cf.B * f);
		}

		public static ColorFactor operator *(float f, ColorFactor cf)
		{
			return new ColorFactor(cf.R * f, cf.G * f, cf.B * f);
		}
	}
}
