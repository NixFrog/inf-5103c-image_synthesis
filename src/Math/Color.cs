﻿namespace ImageSynthesis
{
    public struct Color
    {
        public float R, G, B;	// RGB components between 0 and 1

        public void From255(byte RR, byte GG, byte BB)
        {
            R = (float)(RR / 255.0);
            G = (float)(GG / 255.0);
            B = (float)(BB / 255.0);
        }

        static public void Transpose(ref Color cc, System.Drawing.Color c)
        {
            cc.R = (float) (c.R / 255.0);
            cc.G = (float) (c.G / 255.0);
            cc.B = (float) (c.B / 255.0);
        }

        public void check()
        {
            if (R > 1.0) R = 1.0f;
            if (G > 1.0) G = 1.0f;
            if (B > 1.0) B = 1.0f;
        }

        public void To255(out byte RR, out byte GG, out byte BB)
        {
            RR = (byte)(R * 255);
            GG = (byte)(G * 255);
            BB = (byte)(B * 255);
        }

        public System.Drawing.Color Convertion()
        {
            check();
            byte RR, GG, BB;
            To255(out RR, out GG, out BB);
            return System.Drawing.Color.FromArgb(RR, GG, BB);
        }

        public Color(float R, float G, float B)
        {
            this.R = R;
            this.G = G;
            this.B = B;
        }

        public Color(Color c)
        {
            this.R = c.R;
            this.G = c.G;
            this.B = c.B;
        }

        public float GreyLevel()
        {
            return (R + B + G) / 3.0f;
        }

        public static Color operator +(Color a, Color b)
        {
            return new Color(a.R + b.R, a.G + b.G, a.B + b.B);
        }

        public static Color operator -(Color a, Color b)
        {
            return new Color(a.R - b.R, a.G - b.G, a.B - b.B);
        }

        public static Color operator -(Color a)
        {
            return new Color(-a.R, -a.G, -a.B);
        }

        public static Color operator *(Color a, Color b)
        {
            return new Color(a.R * b.R, a.G * b.G, a.B * b.B);
        }

        public static Color operator *(float a, Color b)
        {
            return new Color(a * b.R, a * b.G, a * b.B);
        }

        public static Color operator *(Color b, float a)
        {
            return new Color(a * b.R, a * b.G, a * b.B);
        }

        public static Color operator /(Color b, float a)
        {
            return new Color(b.R / a, b.G / a, b.B / a);
        }
    }
}

