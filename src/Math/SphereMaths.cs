using System.Collections.Generic;
using static System.Math;
using System;

namespace ImageSynthesis
{
	static class SphereMaths
	{
		public static List<V3> GenerateSphericPoints(V3 spherePosition, int rayCount)
		{
			var pointList = new List<V3>();
			Random random = new Random();

			for(int i = 0; i < rayCount; i++){
				double rand = random.NextDouble();
				double theta = 2 * PI * rand;
				double phi = Acos(2 * rand - 1.0);

				var x = (float)Cos(theta) * (float)Sin(phi);
				var y = (float)Sin(theta) * (float)Sin(phi);
				var z = (float)Cos(phi);
				var pos = new V3(x*100, y*100, z*100);
				pos += spherePosition;

				pointList.Add(pos);
			}

			return pointList;
		}

		public static V3 NormalOfPoint(V3 position, V3 point)
		{
			V3 normal = point - position;
			normal.Normalize();
			return normal;
		}
	}
}
