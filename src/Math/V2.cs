using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImageSynthesis
{
    struct V2
    {
        public float x;
        public float y;

        public float Norm()
        {
            return (float) IMA.Sqrtf(x * x + y * y);
        }

        public float Norme2()
        {
            return x * x + y * y;
        }

        public void Normalize()
        {
            float n = Norm();
            if (n == 0) return;
            x /= n;
            y /= n;
        }

        public V2(V2 t)
        {
            x = t.x;
            y = t.y;
        }

        public V2(float _x, float _y)
        {
            x = _x;
            y = _y;
        }

        V2(int _x, int _y)
        {
            x = (float)_x;
            y = (float)_y;
        }

        public static V2 operator +(V2 a, V2 b)
        {
            V2 t;
            t.x = a.x + b.x;
            t.y = a.y + b.y;
            return t;
        }

        public static V2 operator -(V2 a, V2 b)
        {
            V2 t;
            t.x = a.x - b.x;
            t.y = a.y - b.y;
            return t;
        }

        public static V2 operator -(V2 a)
        {
            V2 t;
            t.x = -a.x;
            t.y = -a.y;
            return t;
        }

        public static float operator *(V2 a,V2 b) // Dot product
        {
            return a.x * b.x + a.y * b.y;
        }

        public static V2 operator *(float a, V2 b)
        {
            V2 t;
            t.x = b.x*a;
            t.y = b.y*a;
            return t;
        }

        public static V2 operator *(V2 b, float a)
        {
            V2 t;
            t.x = b.x*a;
            t.y = b.y*a;
            return t;
        }

        public static V2 operator /(V2 b, float a)
        {
            V2 t;
            t.x = b.x/a;
            t.y = b.y/a;
            return t;
        }

        public static float dot_product(ref V2 u, ref V2 v)
        {
            return u.x * v.x + u.y * v.y;
        }
    }
}
