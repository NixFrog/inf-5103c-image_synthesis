﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImageSynthesis
{
    static class MainProject
    {
        public static void Go()
        {

			V3 ViewPoint = new V3(
					ScreenBitmap.Width / 2
					, -1000
					, ScreenBitmap.Height / 2
					);

			var currentScene = new BoxScene(ViewPoint);

			var rayCastingRenderer = new RayCastingRenderer(currentScene.Phong, ViewPoint);
			var vplRenderer = new VPLRenderer(currentScene.Phong, ViewPoint);

			currentScene.RenderMethod = rayCastingRenderer;
			//currentScene.RenderMethod = vplRenderer;

			currentScene.Render();
        }
    }
}
