using System.Collections.Generic;

namespace ImageSynthesis
{
	class PhongIlluminationModel
	{
		public V3 ViewPoint {get; set;}
		public List<LightSource> LightSources {get; set;}

		public PhongIlluminationModel(V3 viewPoint)
		{
			ViewPoint = viewPoint;
			LightSources = new List<LightSource>();
		}

		public Color LightColor(List<GeometricObject> objectList,
				GeometricObject obj, V3 point, V2 uvCoords)
		{
			var pointColor = new Color(0, 0, 0);
			foreach(var lightSource in LightSources){
				if(lightSource is AmbientLight){
					pointColor += ColorFromSource(
							obj, (AmbientLight)lightSource, point, uvCoords
							);
				}
				else if(lightSource is PointLight){
					var lightS = lightSource as PointLight;
					if(lightS.OriginObject != obj){
						if(IsLightened(objectList, obj, point, (PointLight)lightSource)){
							pointColor += ColorFromSource(
									obj, lightS, point, uvCoords
									);
						}
					}
				}
				else if(lightSource is DirectionalLight){
					if(IsLightened(objectList, obj, point, (DirectionalLight)lightSource)){
						pointColor += ColorFromSource(
								obj, (DirectionalLight)lightSource, point, uvCoords
								);
					}
				}
			}
			return pointColor;
		}

		private Color ColorFromSource(GeometricObject obj
				, AmbientLight lightSource, V3 point, V2 uvCoords)
		{
			return lightSource.LightColor
				* obj.Material.AmbientCoefficient
				* obj.PointColor(uvCoords);
		}

		private Color ColorFromSource(GeometricObject obj
				, DirectionalLight lightSource, V3 point, V2 uvCoords)
		{
			var lightVector = -lightSource.Direction;
			lightVector.Normalize();
			var reflectedColor = ComputeReflection(obj, point, lightVector, lightSource, uvCoords);

			return reflectedColor;
		}

		private Color ColorFromSource(GeometricObject obj
				, PointLight lightSource, V3 point, V2 uvCoords)
		{
			V3 lightVector = lightSource.Position - point;
			lightVector.Normalize();
			var reflectedColor = ComputeReflection(obj, point, lightVector, lightSource, uvCoords);

			return reflectedColor;
		}

		private Color ComputeReflection(GeometricObject obj, V3 point
				, V3 lightVector, LightSource lightSource, V2 uvCoords)
		{
			// Do not light if it is on the wrong side.
			if(obj.NormalOfPoint(point) * lightVector <= 0){
				return new Color(0,0,0);
			}

			var bumpedNormal = obj.BumpedNormal(point, uvCoords);

			var reflectedVector = lightVector.ReflectVector(bumpedNormal);
			reflectedVector.Normalize();

			V3 viewVector = ViewPoint - point;
			viewVector.Normalize();

			var diffuseComponent = DiffuseComponent(obj.Material, lightVector, bumpedNormal);
			var specularComponent = SpecularComponent(obj.Material, reflectedVector, viewVector);

			return (diffuseComponent + specularComponent) * lightSource.LightColor;
		}

		private ColorFactor DiffuseComponent(Material mat, V3 lightVector, V3 normalVector)
		{
			var diffuseComponent = new ColorFactor(0,0,0);
			float product = lightVector * normalVector;
			if(product > 0.0f){
				diffuseComponent = product * mat.DiffuseCoefficient;
			}

			return diffuseComponent;
		}

		private ColorFactor SpecularComponent(Material mat, V3 reflectedVector, V3 viewVector)
		{
			var specularComponent = new ColorFactor(0,0,0);
			var product = reflectedVector * viewVector;
			if(product > 0.0f){
				specularComponent = mat.SpecularCoefficient
					* (float)System.Math.Pow(product, mat.Shininess);
			}
			return specularComponent;
		}


		private bool IsLightened(List<GeometricObject> objectList
				, GeometricObject obj, V3 point, DirectionalLight lightSource)
		{
			float intersectingDistance;
			Beam beam = new Beam(point, - lightSource.Direction, obj);
			return (beam.FirstIntersectedObject(objectList, out intersectingDistance) == null);
		}

		private bool IsLightened(List<GeometricObject> objectList
				, GeometricObject obj, V3 point, PointLight lightSource)
		{
			float intersectingDistance;
			Beam beam = new Beam(point, lightSource.Position - point, obj);

			bool isLightened = (
					beam.FirstIntersectedObject(
						objectList
						, out intersectingDistance
					) == null
					);

			float distanceToLight = (lightSource.Position - point).Norm();
			//System.Console.WriteLine("dobj "+distanceToLight.ToString() + " intD " + intersectingDistance.ToString());

			if(distanceToLight < intersectingDistance*100){
				return isLightened;
			}
			else{
				return false;
			}
		}
	}
}
