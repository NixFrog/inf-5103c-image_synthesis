namespace ImageSynthesis
{
	class PointLight
		: LightSource
	{
		public Color LightColor { get; set; }
		public GeometricObject OriginObject {get; set;}
		public V3 Position { get; set; }

		public PointLight(Color lightColor, V3 position, GeometricObject obj = null)
		{
			LightColor = lightColor;
			Position = position;
			OriginObject = obj;
		}
	}
}
