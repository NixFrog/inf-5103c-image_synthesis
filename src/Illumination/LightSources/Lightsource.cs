namespace ImageSynthesis
{
	interface LightSource
	{
		Color LightColor { get; set;}
	}
}
