namespace ImageSynthesis
{
	class DirectionalLight
		: LightSource
	{
		public Color LightColor { get; set; }
		public V3 Direction { get; set; }

		public DirectionalLight(Color lightColor, V3 direction)
		{
			LightColor = lightColor;
			Direction = direction;
			Direction.Normalize();
		}
	}
}
