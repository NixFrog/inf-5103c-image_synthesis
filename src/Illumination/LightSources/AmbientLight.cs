namespace ImageSynthesis
{
	class AmbientLight
		: LightSource
	{
		public Color LightColor{get; set;}

		public AmbientLight(Color lightColor)
		{
			LightColor = lightColor;
		}
	}
}
