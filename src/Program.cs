﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ImageSynthesis
{
    static class Program
    {
        static public Form1 FirstForm;

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            FirstForm = new Form1();

            Application.Run(FirstForm);
        }
    }
}
