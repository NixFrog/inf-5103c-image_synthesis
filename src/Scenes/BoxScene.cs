namespace ImageSynthesis
{
	class BoxScene
		: Scene
	{
		public BoxScene(V3 viewPoint)
			: base(viewPoint)
		{
			CreateWalls();
			CreateSpheres();
			CreateLights();
		}

		private void CreateLights()
		{
			var ambientLight = new AmbientLight(new Color(0.3f, 0.3f, 0.3f));

			var directionalLight = new DirectionalLight(new Color(0.4f, 0.4f, 0.4f), new V3(-1, 1, 1));

			var pointLight = new PointLight(new Color(0.9f, 0.9f, 0.9f)
					, new V3(ScreenBitmap.Width/2, -100, 400));

			var pointLight2 = new PointLight(new Color(0.5f, 0.5f, 0.5f)
					, new V3(100, -100, 400));

			var pointLight3 = new PointLight(new Color(0.5f, 0.5f, 0.5f)
					, new V3(800, -100, 400));

			Phong.LightSources.Add(ambientLight);
			Phong.LightSources.Add(directionalLight);
			Phong.LightSources.Add(pointLight);
			Phong.LightSources.Add(pointLight2);
			Phong.LightSources.Add(pointLight3);
		}

		private void CreateSpheres()
		{
			var sphere = new Sphere(
					new V3(200,350,400)
					, 100
					, MaterialInstances.Materials["lead"]
					);

			var sphere2 = new Sphere(
					new V3(ScreenBitmap.Width - 200,350,400)
					, 100
					, MaterialInstances.Materials["gold"]
					);

			GeometricObjects.Add(sphere);
			GeometricObjects.Add(sphere2);
		}

		private void CreateWalls()
		{
			var width = ScreenBitmap.Width;
			var height = ScreenBitmap.Height;
			var depth = 500;

			var backP  = new V3(width/2, depth,   height/2);
			var leftP  = new V3(0,		 depth/2, height/2);
			var rightP = new V3(width,	 depth/2, height/2);
			var downP  = new V3(width/2, depth/2, height);
			var upP    = new V3(width/2, depth/2, 0);

			var back = new Rectangle(
					backP
					, new V3(width,0,0)
					, new V3(0,0,height)
					, MaterialInstances.Materials["white_wall"]
					);

			var left = new Rectangle(
					leftP
					, new V3(0,height,0)
					, new V3(0,0,height)
					, MaterialInstances.Materials["white_wall"]
					);

			var right = new Rectangle(
					rightP
					, new V3(0, -height, 0)
					, new V3(0, 0, height)
					, MaterialInstances.Materials["white_wall"]
					);

			var up = new Rectangle(
					upP
					, new V3(-width, 0, 0)
					, new V3(0,-depth,0)
					, MaterialInstances.Materials["white_ceiling"]
					);

			var down = new Rectangle(
					downP
					, new V3(-width,0,0)
					, new V3(0,depth,0)
					, MaterialInstances.Materials["wood"]
					);

			GeometricObjects.Add(back);
			GeometricObjects.Add(left);
			GeometricObjects.Add(right);
			GeometricObjects.Add(up);
			GeometricObjects.Add(down);
		}
	}
}
