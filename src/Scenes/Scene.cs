using System.Collections.Generic;

namespace ImageSynthesis
{
	class Scene
	{
		public List<GeometricObject> GeometricObjects {get; set;}
		public Renderer RenderMethod{get; set;}
		public PhongIlluminationModel Phong { get; set; }

		public Scene()
		{
			RenderMethod = new ZBufferRenderer();
			GeometricObjects = new List<GeometricObject>();
		}

		public Scene(V3 viewPoint)
			: this()
		{
			Phong = new PhongIlluminationModel(viewPoint);
		}

		public Scene(PhongIlluminationModel phong)
			: this()
		{
			Phong = phong;
		}

		public void Render()
		{
			RenderMethod.Render(GeometricObjects, Phong);
		}
	}
}
