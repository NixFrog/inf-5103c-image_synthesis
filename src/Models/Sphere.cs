using System;

namespace ImageSynthesis
{
	class Sphere
		: GeometricObject
	{
		public float Radius {get; set;}

		public Sphere()
		{
			Position = new V3(0.0f, 0.0f, 0.0f);
			Radius = 100;
		}

		public Sphere(V3 position, float radius, Material material)
		{
			Position = position;
			Radius = radius;
			Material = material;
		}

		override public V3 GetPointFromUV(V2 uv)
		{
			var sc = SurfaceCoordinatesFromUV(uv);
			var x = Position.x + Radius * (float)Math.Cos(sc.x) * (float)Math.Cos(sc.y);
			var y = Position.y + Radius * (float)Math.Sin(sc.x) * (float)Math.Cos(sc.y);
			var z = Position.z + Radius * (float)Math.Sin(sc.y);
			return new V3(x, y, z);
		}

		override public V2 SurfaceCoordinatesFromUV(V2 uv)
		{
			return new V2(
				uv.x * 2 * (float)Math.PI
				, (uv.y - 0.5f) * (float)Math.PI
				);
		}

		override public V2 UVCoordinatesFromPoint(V3 point)
		{
			float u, v;
			V3 direction = point - Position;
			u = (float)Math.Atan2(direction.y, direction.x);
			v = (float)Math.Asin(direction.z / Radius);

			return new V2(
					u / (2 * (float)Math.PI)
					, 0.5f + v / (float)Math.PI
					);
		}

		override public V3 NormalOfPoint(V3 point)
		{
			return SphereMaths.NormalOfPoint(Position, point);
		}

		override public void DerivePointUV(V2 uvCoords, out V3 dMdu, out V3 dMdv)
		{
			float u = uvCoords.x;
			float v = uvCoords.y;

			dMdu = new V3(
					- Radius * (float)Math.Sin(u) * (float)Math.Cos(v)
					, - Radius * (float)Math.Sin(u) * (float)Math.Sin(v)
					, Radius * (float)Math.Cos(u)
					);

			dMdv = new V3(
					- Radius * (float)Math.Cos(u) * (float)Math.Sin(v)
					, Radius * (float)Math.Cos(u) * (float)Math.Sin(v)
					, 0
					);

			dMdu.Normalize();
			dMdv.Normalize();
		}

		override public bool IsIntersectingRay(Beam beam, out float distance)
		{
			distance = float.MaxValue;
			V3 toOrigin = beam.Origin - Position;

			float a = beam.Direction.Norme2();
			float b = 2 * toOrigin * beam.Direction;
			float c = toOrigin.Norme2() - (Radius * Radius);

			float delta = b * b - 4 * c*a;

			if(delta >= 0){
				float sqrtDelta = (float)Math.Sqrt(delta);
				float t1 = (-b + sqrtDelta) / (2*a);
				float t2 = (-b - sqrtDelta) / (2*a);

				if(t1 < 0 || t2 < 0){
					distance = Math.Max(t1, t2);
				}
				else{
					distance = Math.Min(t1, t2);
				}

				return true;
			}
			return false;
		}
	}
}
