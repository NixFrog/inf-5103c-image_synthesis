namespace ImageSynthesis
{
	abstract class GeometricObject
	{
		public Material Material {get; set;}
		public V3 Position {get; set;}

		abstract public V2 SurfaceCoordinatesFromUV(V2 uv);
		abstract public V2 UVCoordinatesFromPoint(V3 point);
		abstract public V3 GetPointFromUV(V2 uv);
		abstract public V3 NormalOfPoint(V3 point);

		public Color PointColor(V2 uv)
		{
			if(Material.Texture == null){
				return Material.Color;
			}
			return Material.Texture.ReadColor(uv);
		}

		public V3 BumpedNormal(V3 point, V2 uvCoords)
		{
			float dhdu, dhdv;

			if(Material.BumpMap == null){
				return NormalOfPoint(point);
			}

			Material.BumpMap.Bump(uvCoords.x, uvCoords.y, out dhdu, out dhdv);
			V3 dMdu, dMdv;
			DerivePointUV(uvCoords, out dMdu, out dMdv);
			V3 normal = NormalOfPoint(point);

			V3 bumpedNormal = normal + (Material.BumpCoefficient * (
				( dMdu ^ normal * dhdv)
				+ (dhdu * normal ^ dMdv)
				));
			bumpedNormal.Normalize();
			return bumpedNormal;
		}

		abstract public void DerivePointUV(V2 uvCoords, out V3 dMdu, out V3 dMdv);
		abstract public bool IsIntersectingRay(Beam beam, out float distance);
	}
}
