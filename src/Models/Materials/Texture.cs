﻿using System.Drawing;
using System.Drawing.Imaging;

namespace ImageSynthesis
{
    class Texture
    {
        public readonly int Height;
        public readonly int Width;
        public readonly Color[,] C;

		public Texture(string filepath)
		{
			Bitmap B = OpenAsBitmap(filepath);

            Height = B.Height;
            Width = B.Width;
            BitmapData data = B.LockBits(
					new System.Drawing.Rectangle(0, 0, B.Width, B.Height)
					, ImageLockMode.ReadWrite
					, PixelFormat.Format24bppRgb
					);
            int stride = data.Stride;

            C = new Color[Width, Height];

            unsafe
            {
                byte* ptr = (byte*)data.Scan0;
                for (int x = 0; x < Width; x++)
                    for (int y = 0; y < Height; y++)
                    {
                        byte RR, GG, BB;
                        BB = ptr[(x * 3) + y * stride];
                        GG = ptr[(x * 3) + y * stride + 1];
                        RR = ptr[(x * 3) + y * stride + 2];
                        C[x, y].From255(RR, GG, BB);
                    }
            }
            B.UnlockBits(data);
            B.Dispose();
		}

		public Bitmap OpenAsBitmap(string filepath)
		{
			string path = System.IO.Path.Combine(
				System.IO.Path.GetFullPath("resources")
				, "textures"
				, filepath
			);
			return new Bitmap(path);
		}

		public Color ReadColor(V2 uv){
			return ReadColor(uv.x, uv.y);
		}

        public Color ReadColor(float u, float v)
        {
            return Interpolate(Width * u, Height * v);
        }

        public void Bump(float u, float v, out float dhdu, out float dhdv)
        {
            float x = u * Height;
            float y = v * Width;

            float vv = Interpolate(x, y).GreyLevel();
            float vx = Interpolate(x + 1, y).GreyLevel();
            float vy = Interpolate(x, y + 1).GreyLevel();

            dhdu = vx - vv;
            dhdv = vy - vv;
        }

        private Color Interpolate(float Lu, float Hv)
        {
            int x = (int)(Lu);
            int y = (int)(Hv);

            x = x % Width;
            y = y % Height;
            if (x < 0) x += Width;
            if (y < 0) y += Height;

            return C[x, y];
        }
    }
}
