/*
 * Some of the values used here come from
 * OpenGL teapots.c demo, © Silicon Graphics, Inc., © 1994, Mark J. Kilgard
 */
using System.Collections.Generic;

namespace ImageSynthesis
{
	static class MaterialInstances
	{
		public static readonly Dictionary<string, Material> Materials
			= new Dictionary<string, Material>
			{
				{ "gold", new Material (
					0.5f
					, new ColorFactor(0.24725f, 0.1995f, 0.0745f)
					, new ColorFactor(0.75164f, 0.60648f, 0.22648f)
					, new ColorFactor(0.628281f, 0.555802f, 0.366065f)
					, 64f
					, new Color(0, 0, 0)
					, new Texture("gold_bump.jpg")
					, new Texture("gold.jpg")
				)},

				{ "lead", new Material (
					1.0f
					, new ColorFactor(0.25f, 0.25f, 0.25f)
					, new ColorFactor(0.4f, 0.4f, 0.4f)
					, new ColorFactor(0.774597f, 0.774597f, 0.774597f)
					, 128f
					, new Color(0, 0, 0)
					, new Texture("lead_bump.jpg")
					, new Texture("lead.jpg")
				)},

				{ "pearl", new Material (
					1.0f
					, new ColorFactor(0.25f, 0.20725f, 0.20725f)
					, new ColorFactor(1f, 0.829f, 0.829f)
					, new ColorFactor(0.296648f, 0.296648f, 0.296648f)
					, 64f
					, new Color(1f, 0.829f, 0.829f)
				)},

				{ "white_ceiling", new Material (
					1.0f
					, new ColorFactor(1f, 1f, 1f)
					, new ColorFactor(0.8f, 0.8f, 0.8f)
					, new ColorFactor(0.3f, 0.3f, 0.3f)
					, 8f
					, new Color(0.75f, 0.75f, 0.75f)
				)},

				{ "white_wall", new Material (
					1.0f
					, new ColorFactor(1f, 1f, 1f)
					, new ColorFactor(0.8f, 0.8f, 0.8f)
					, new ColorFactor(0.1f, 0.1f, 0.1f)
					, 4f
					, new Color(0.55f, 0.55f, 0.55f)
					, new Texture("bump38.jpg")
				)},

				{ "wood", new Material (
					1.0f
					, new ColorFactor(0.5f, 0.5f, 0.5f)
					, new ColorFactor(0.4f, 0.4f, 0.4f)
					, new ColorFactor(0.3f, 0.3f, 0.3f)
					, 8f
					, new Color(0.4f, 0.4f, 0.4f)
					, null
					, new Texture("wood.jpg")
				)},

				{"red_demo", new Material (
					1.0f
					, new ColorFactor(1f, 1f, 1f)
					, new ColorFactor(1f, 1f, 1f)
					, new ColorFactor(1f, 1f, 1f)
					, 64f
					, new Color(0.7f, 0.1f, 0.1f)
				)}
			};
	}
}
