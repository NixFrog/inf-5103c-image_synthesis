namespace ImageSynthesis
{
	class Material
	{
		public Texture Texture {get; set;}
		public Texture BumpMap {get; set;}
		public float BumpCoefficient {get; set;} = 1.0f;
		public ColorFactor AmbientCoefficient {get; set;}
		public ColorFactor DiffuseCoefficient {get; set;}
		public ColorFactor SpecularCoefficient {get; set;}
		public float Shininess {get; set;}
		public Color Color {get; set;}

		public Material( float bumpCoef, ColorFactor ambientCoef
				, ColorFactor diffCoef, ColorFactor specCoef, float shiny
				, Color color
				, Texture bumpMap = null
				, Texture texture = null)
		{
			Texture = texture;
			BumpMap = bumpMap;
			BumpCoefficient = bumpCoef;
			AmbientCoefficient = ambientCoef;
			DiffuseCoefficient = diffCoef;
			SpecularCoefficient = specCoef;
			Shininess = shiny;
			Color = color;
		}
	}
}
