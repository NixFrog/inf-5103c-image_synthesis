﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImageSynthesis
{
    enum DisplayMode { SLOW_MODE, FULL_SPEED};

    class ScreenBitmap
    {
        const int RefhreshCount = 1000;
        static int PixelCount = 0;

        static private System.Drawing.Bitmap B;
        static private DisplayMode Mode;
        static public int Width { get; set; }
        static public int Height { get; set; }
        static private int Stride;
        static private System.Drawing.Imaging.BitmapData data;

        static public System.Drawing.Bitmap Init(int width, int height)
        {
            Width = width;
            Height = height;
            B = new System.Drawing.Bitmap(width, height);
            return B;
        }

        static void DrawFastPixel(int x, int y, Color c)
        {
            unsafe
            {
                byte RR, VV, BB;
                c.check();
                c.To255(out RR, out  VV, out  BB);

                byte* ptr = (byte*)data.Scan0;
                ptr[(x * 3) + y * Stride    ] = BB;
                ptr[(x * 3) + y * Stride + 1] = VV;
                ptr[(x * 3) + y * Stride + 2] = RR;
            }
        }

        static void DrawSlowPixel(int x, int y, Color c)
        {
            System.Drawing.Color cc = c.Convertion();
            B.SetPixel(x, y, cc);

            Program.FirstForm.PictureBoxInvalidate();
            PixelCount++;
            if (PixelCount > RefhreshCount)
            {
               Program.FirstForm.PictureBoxRefresh();
               PixelCount = 0;
            }
         }

        static public void RefreshScreen(Color c)
        {
            if (Program.FirstForm.Checked())
            {
                Mode = DisplayMode.SLOW_MODE;
                System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(B);
                System.Drawing.Color cc = c.Convertion();
                g.Clear(cc);
            }
            else
            {
                Mode = DisplayMode.FULL_SPEED;
				data = B.LockBits(
							new System.Drawing.Rectangle(0, 0, B.Width, B.Height)
							, System.Drawing.Imaging.ImageLockMode.ReadWrite
							, System.Drawing.Imaging.PixelFormat.Format24bppRgb
						);
				Stride = data.Stride;
                for (int x = 0; x < Width; x++){
                    for (int y = 0; y < Height; y++){
                        DrawFastPixel(x, y, c);
					}
				}
			}
        }

        public static void DrawPixel(int x, int y, Color c)
        {
            int x_screen = x;
            int y_screen = Height - y;

            if ((x_screen >= 0) && (x_screen < Width) && (y_screen >= 0) && (y_screen < Height))
                if (Mode == DisplayMode.SLOW_MODE) DrawSlowPixel(x_screen, y_screen, c);
                else DrawFastPixel(x_screen, y_screen, c);
        }

        static public void Show()
        {
            if (Mode == DisplayMode.FULL_SPEED)
                B.UnlockBits(data);

            Program.FirstForm.PictureBoxInvalidate();
        }

        static public int GetWidth() { return Width; }
        static public int GetHeight() { return Height; }
    }
}
