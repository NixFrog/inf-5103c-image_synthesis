namespace ImageSynthesis
{
	class Rectangle
		: GeometricObject
	{
		public V3 WidthVector {get; set;}
		public V3 HeightVector {get; set;}

		public Rectangle(V3 position, V3 widthVector, V3 heightVector, Material material)
		{
			Position = position;
			WidthVector = widthVector;
			HeightVector = heightVector;
			Material = material;
		}

		override public V2 SurfaceCoordinatesFromUV(V2 uv)
		{
			return new V2(uv.x - 0.5f, uv.y - 0.5f);
		}

		override public V2 UVCoordinatesFromPoint(V3 point)
		{
			V3 toPosition = point - Position;
			V3 wvNormed = new V3(WidthVector);
			wvNormed.Normalize();
			V3 hvNormed = new V3(HeightVector);
			hvNormed.Normalize();

			float u = ((toPosition * wvNormed) / WidthVector.Norm());
			float v = ((toPosition * hvNormed) / HeightVector.Norm());

			u += 0.5f;
			v += 0.5f;
			return new V2(u, v);
		}

		override public V3 GetPointFromUV(V2 uv)
		{
			return Position + uv.x * WidthVector + uv.y * HeightVector;
		}

		override public V3 NormalOfPoint(V3 point)
		{
			var normal = WidthVector ^ HeightVector;
			normal.Normalize();
			return normal;
		}

		override public void DerivePointUV(V2 uvCoords, out V3 dMdu, out V3 dMdv)
		{
			dMdu = WidthVector;
			dMdu.Normalize();
			dMdv = HeightVector;
			dMdv.Normalize();
		}

		override public bool IsIntersectingRay(Beam beam, out float distance)
		{
			distance = float.MaxValue;
			V3 toOrigin = Position - beam.Origin;
			var normal = NormalOfPoint(beam.Origin);

			float t = (toOrigin * normal) / (beam.Direction * normal);

			if(t > 0){
				distance = t;
				V3 collisionPoint = beam.Origin + (beam.Direction * distance);
				V3 corner = Position - WidthVector / 2 - HeightVector / 2;
				V3 v = collisionPoint - corner;

				bool isInRectangle = ( WidthVector * v >= 0
									&& WidthVector * v <= WidthVector.Norme2()
									&& HeightVector * v >= 0
									&& HeightVector * v <= HeightVector.Norme2()
									);
				return isInRectangle;
			}
			return false;
		}
	}
}
