MCS = mcs

MCSFLAGS = -pkg:dotnet -lib:/usr/lib/mono/4.5 -unsafe

SRCFILES = $(shell find src/ -type f -name "*.cs")
BUILDDIR = build/3D_synthesis_project

.PHONY: build clean

build:
	@mkdir -p build
	@$(MCS) $(MCSFLAGS) $(SRCFILES) -out:$(BUILDDIR)

clean:
	@rm -f build/*

